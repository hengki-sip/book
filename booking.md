## REQUEST HEADERS

```dart
// BUILDING
// user.unit != null
X-User-Building
X-User-Building-Unit

// LOCATION
// device.location != null
X-Latitude
X-Longitude

// USER
X-User-Email
X-User-ID
X-User-Phone
```





## BOOKINGS

```json
[
    {
        "id": "1",
        "status": "Booked",
        "booking_number": "Y1231175576",
        "date": "2022-10-09 20:06:01.362116",
        "check_in_schedule": "2022-10-18 13:00:00.000",
        "check_out_schedule": "2022-10-18 14:00:00.000",
        "qty": 1,
        "facility": {
            "id": "1",
            "image": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png",
            "name": "Stenella coeruleoalba"
        },
        "user_status": {
            "check_in_time": null,
            "check_out_time": null,
            "canceled_time": null
        }
    }
]
```



## CREATE BOOKING
body :
```json
{
    "facility_id" : "123123",
    "schedule_time": "2022-10-18 13:00:00.000",
    "qty" : 1,

}
```
response : 

```json
{
    "id": "2",
    "status": "Booked",
    "booking_number": "Y1231175576",
    "date": "2022-10-09 20:06:01.362116",
    "check_in_schedule": "2022-10-18 13:00:00.000",
    "check_out_schedule": "2022-10-18 14:00:00.000",
    "qty": 1,
    "facility": {
        "id": 1,
        "image": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png",
        "name": "Stenella coeruleoalba"
    },
    "user_status": {
        "check_in_time": null,
        "check_out_time": null,
        "canceled_time": null
    }
}

```



## FACILITIES
```json
[
    {
        "id": "1",
        "name": "Stenella coeruleoalba",
        "open": "09-03-2022",
        "img": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png"
    },
    {
        "id": "2",
        "name": "Tragelaphus strepsiceros",
        "open": "02-02-2022",
        "img": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png"
    },
    {
        "id": "3",
        "name": "Cordylus giganteus",
        "open": "08-07-2022",
        "img": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png"
    }
]
```
 
## FACILITY DETAIL

    open_schedule harus 7 hari

```json
{
    "id": "1",
    "images": [
        "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png"
    ],
    "name": "Stenella coeruleoalba",
    "location": "Bussness Park D2 - 3",
    "duration": "1 hour",
    "open_schedule": [
        {
            "day": "Sunday",
            "schedule": "10:00 - 21:00",
            "used_quota": 88,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Monday",
            "schedule": "10:00 - 21:00",
            "used_quota": 88,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Tuesday",
            "schedule": "11:00 - 21:00",
            "used_quota": 88,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Wednesday",
            "schedule": "08:00 - 21:00",
            "used_quota": 100,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Thursday",
            "schedule": "13:00 - 21:00",
            "used_quota": 100,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Friday",
            "schedule": "13:00 - 21:00",
            "used_quota": 100,
            "total_quota": 100,
            "is_open": true
        },
        {
            "day": "Saturday",
            "schedule": "10:00 - 21:00",
            "used_quota": 100,
            "total_quota": 100,
            "is_open": false
        }
    ],
    "description": "Gold's Gym Indonesia has been operating under the corporate representation of PT Fit and Health Indonesia since 2007. Our gyms are built to help people realize their goals and find their inner strength. Most importantly, you'll find an energetic, supportive environment full of all kinds of people who are committed to achieve their goals. Since 1965 Gold’s Gym have been the recognized authority in fitness industry, and now, every time you come to our clubs, you will be able to experience why nearly 3 million members at over 750 locations in 35 countries around the worlds trust Gold's Gym for their health and fitness. From the best trainers, equipment and programs to the best group exercise classes on this planet.",
    "supported_facilities": [
        {
            "id": "1",
            "name": "Toilet",
            "img": "https://i.postimg.cc/Ssw-hv1nP/icons8-wi-fi-100.png"
        },
        {
            "id": "2",
            "name": "Wifi",
            "img": "https://i.postimg.cc/Ssw-hv1nP/icons8-wi-fi-100.png"
        },
        {
            "id": "3",
            "name": "Towel",
            "img": "https://i.postimg.cc/Ssw-hv1nP/icons8-wi-fi-100.png"
        }
    ],
    "rules": [
        "Mengikuti aturan protokol kesehatan",
        "Dilarang membawa hewan peliharaan.",
        "Dilarang merokok"
    ]
}
```

## CHECK IN
body :
```json
{
    "booking_number" : "123123",
    "check_in_time": "2022-10-18 13:00:00.000"
}
```
response : 

```json
    {
        "id": "1",
        "status": 2 ,
        "booking_number": "Y1231175576",
        "date": "2022-10-09 20:06:01.362116",
        "check_in_schedule": "2022-10-18 13:00:00.000",
        "check_out_schedule": "2022-10-18 14:00:00.000",
        "qty": 1,
        "facility": {
            "id": "1",
            "image": "https://i.postimg.cc/hGYYxdyT/istockphoto-1132006407-612x612-1-2-2.png",
            "name": "Stenella coeruleoalba"
        },
        "user_status": {
            "check_in_time": "User CHECK OUT DATE TIME",
            "check_out_time": null,
            "canceled_time": null
        }
    }

```

## CHECK OUT
body :
```json
{
    "booking_number" : "123123",
    "check_out_time": "2022-10-18 13:00:00.000"
}
```
response : 

```json
{
  "status" : "ok"
}
```


## CANCELED
body :
```json
{
    "booking_number" : "123123",
    "canceled_time": "2022-10-18 13:00:00.000"
}
```
response : 

```json
{
    "status" : "ok"
}
```
